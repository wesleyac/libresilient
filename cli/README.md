# LibResilient CLI

This is the command-line interface for LibResilient. It is supposed to understand the `config.json` configuration file, and simplify deployment of content so as to ensure that all configured transport plugins work as expected. It should also *test* if those channels work as expected.

This is a work in progress and not production-ready yet.

